//import
const express = require('express');
const exphb = require('express-handlebars');
const path = require('path');

// init
const app = express();
const APP_PORT = process.env.PORT;

app.engine('hbs', exphb());
app.set('view engine', 'hbs');
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.urlencoded());

var cartArr = [];

app.get('/empty-cart', (req,res,next)=>{
    cartArr = [];
    res.redirect('/add-cart');
})

app.get('/add-cart',(req,res, next)=>{
    console.log("add to cart");
    let product1 = {
        name: 'Milo',
        quantity: '10',
        unitPrice: '9'
    }
    let product2 = {
        name: 'Ovaltine',
        quantity: '10',
        unitPrice: '8'
    }
    
    cartArr.push(product1);
    cartArr.push(product2);
    let totalAmount = (product1.quantity * product1.unitPrice) +
            (product2.quantity * product2.unitPrice)
    console.log(cartArr.length <= 0);
    console.log("totalAmount > " + totalAmount);
    res.render('shopping-cart', 
            {cartArr: cartArr, 
            isEmpty: cartArr.length <= 0,
            totalAmount: totalAmount
        });
});

app.post('/checkout', (req, res , next)=>{
    console.log("Checkout");
    //console.log(req.body);
    console.log(req.body.totalAmount);
    res.render('checkout');
});

app.use((req, res, next)=>{
    res.send("<b>ERROR !</b>")
})

app.listen(APP_PORT, ()=>{
    console.log(`App Server started on ${APP_PORT}`)
})